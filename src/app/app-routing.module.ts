import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { TenantService, Tenant } from './shared/tenant/tenant.service';

const routes: Routes = [
  // {
  //   path: 'wizard',
  //   loadChildren: () => import('./pages/wizard/wizard.module').then(m => m.WizardModule)
  // },
  // {
  //   path: '',
  //   redirectTo: 'wizard', 
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(private router: Router, private TenantService: TenantService) {
    console.log('contruct into app-routing');
    let appRoutes: Routes = [];
    if ( [Tenant.CLIENT1, Tenant.CLIENT2].indexOf(this.TenantService.getTenant()) !== -1) {
      console.log('config routes to tenant general');
      appRoutes = [
        {
          path: '',
          loadChildren: () => import(
            './pages/wizard/wizard.module').then(m => m.WizardModule),
        }
      ];
    } else if (this.TenantService.getTenant() === Tenant.CLIENT3) {
      console.log('config rotes to tenant', Tenant.CLIENT3);
      appRoutes = [
        {
          path: '',
          loadChildren: () => import(
            './pages/wizard-custom/wizard-custom.module').then(m => m.WizardCustomModule),
        }
      ];
    }
    appRoutes.forEach(e => this.router.config.unshift(e));
    console.log('routes into app-routing.module', this.router);
  }

 }
