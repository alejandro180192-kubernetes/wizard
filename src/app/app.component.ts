import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Tenant, TenantService } from './shared/tenant/tenant.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './app.component.skins.scss']
})
export class AppComponent implements OnInit {
  title = 'testAngular8';

  constructor(private router: Router, private tenantService: TenantService) {
    console.log('contruct into app.component');
    
    // const clientId = (<any>window).clientId;
    // console.log("clientId", clientId);

    // if(clientId){
    //   this.router.navigateByUrl('/wizard/step3')
    // }
  }

  @HostBinding("class.theme-client1") public client1Theme: boolean;
  @HostBinding("class.theme-client2") public client2Theme: boolean;

  ngOnInit() {
    console.log('init app.component');
    
    this.enableThemes();
  }

  get tenant() : string {
    return this.tenantService.getTenant();
  }

  enableThemes() {
    this.client1Theme = this.tenantService.getTenant() === Tenant.CLIENT1;
    this.client2Theme = this.tenantService.getTenant() === Tenant.CLIENT2;
    console.log('enablethemes', this.client1Theme, this.client2Theme);
  }
  
}
