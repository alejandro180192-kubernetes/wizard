import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';
import { TopBarComponent } from './top-bar/top-bar.component';


@NgModule({
  declarations: [BottomBarComponent, TopBarComponent],
  imports: [
    CommonModule,
    ComponentsRoutingModule
  ],
  exports: [BottomBarComponent, TopBarComponent]
})
export class ComponentsModule { }
