import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-wizard-step3',
  templateUrl: './wizard-step3.component.html',
  styleUrls: ['./wizard-step3.component.scss']
})
export class WizardStep3Component implements OnInit {
  step3Form: FormGroup;
  showScheduleForm: boolean;
  disableApplyToAllBtn: boolean;

  days = [
    'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'
  ]
  constructor(
    private router: Router,
    private location: Location,
    private formBuilder: FormBuilder) {

  }

  ngOnInit() {
    this.step3Form = this.formBuilder.group({
      products: new FormArray([]),
      deliveryType: new FormControl('2'),
      paymentMethods: new FormGroup({
        visa: new FormControl(false),
        mcard: new FormControl(false),
        amex: new FormControl(false),
        check: new FormControl(false),
        cash: new FormControl(false)
      }),
      scheduleType: new FormControl('1'),
      schedule: this._createScheduleGroup(),
      extraInfo: new FormControl(''),
      description: new FormControl('')
    });

    this.addProductField();

    this.showScheduleForm = this.step3Form.get('scheduleType').value == 1;
    this.disableApplyToAllBtn = this.d.controls[0].value[`from${0}`] == '' || this.d.controls[0].value[`to${0}`] == ''
  }

  private _createScheduleGroup() {
    let rows = [];
    for (let i = 0; i < 7; i++) {
      rows.push(
        this.formBuilder.group({
          [`from${i}`]: ['', Validators.required],
          [`to${i}`]: ['', Validators.required],
          [`closed${i}`]: [false],
        })
      )
    }
    return new FormArray(rows)
  }

  // convenience getters for easy access to form fields
  get f() { return this.step3Form.controls; }
  get t() { return this.f.products as FormArray; }
  get d() { return this.f.schedule as FormArray; }

  addProductField() {
    this.t.push(this.formBuilder.group({
      description: ['', Validators.required],
    }));
  }

  showSchedule(show: boolean) {
    this.showScheduleForm = show;

    if (!show)
      this.step3Form.get('schedule').disable()
    else
      this.step3Form.get('schedule').enable()
  }

  closedOn(index: number) {
    this.d.controls[index].patchValue({
      [`from${index}`]: !this.d.controls[index].value.closed ? '00:00' : '',
      [`to${index}`]: !this.d.controls[index].value.closed ? '00:00' : '',
    })

    if (index == 0)
      this.disableApplyToAllBtn = this.d.controls[0].value[`from${0}`] == '' || this.d.controls[0].value[`to${0}`] == ''
  }

  onHoursChanged(index: number, input: any) {
    
    this.disableApplyToAllBtn = !input['from0'] || !input['to0'];

    if((input[`from${index}`] && input[`from${index}`] != '00:00') || (input[`to${index}`] && input[`to${index}`] != '00:00')){
      this.d.controls[index].patchValue({
        [`closed${index}`]: false
      })
    }
  }

  applyToAll() {
    this.d.controls.forEach((c, idx) => {
      c.patchValue({
        [`from${idx}`]: this.d.controls[0].value['from0'],
        [`to${idx}`]: this.d.controls[0].value['to0'],
        [`closed${idx}`]: this.d.controls[0].value['closed0'],
      })
    })
  }

  cancel() {
    this.location.back();
  }

  continue() {
    console.log(this.step3Form.value);

    this.router.navigateByUrl('/wizard/step4')
  }
}