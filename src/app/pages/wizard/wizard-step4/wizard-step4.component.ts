import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wizard-step4',
  templateUrl: './wizard-step4.component.html',
  styleUrls: ['./wizard-step4.component.scss']
})
export class WizardStep4Component {
  constructor(private router: Router){
  }

  continue(){
    this.router.navigateByUrl('/wizard/step1')
  }
}