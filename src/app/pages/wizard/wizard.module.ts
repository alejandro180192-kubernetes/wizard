import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card'
import { MatButtonModule } from '@angular/material/button'
import {MatGridListModule} from '@angular/material/grid-list';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';
import { MatIconModule} from '@angular/material/icon';
import { MatRadioModule} from '@angular/material/radio';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSelectModule} from '@angular/material/select';

import { WizardComponent } from './wizard.component';
import { WizardStep1Component } from './wizard-step1/wizard-step1.component';
import { WizardRoutingModule } from './wizard-routing.module';
import { WizardStep2Component } from './wizard-step2/wizard-step2.component';
import { WizardStep3Component } from './wizard-step3/wizard-step3.component';
import {MatStepperModule} from '@angular/material/stepper';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WizardStep4Component } from './wizard-step4/wizard-step4.component';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    WizardComponent,
    WizardStep1Component,
    WizardStep2Component,
    WizardStep3Component,
    WizardStep4Component
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    WizardRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatStepperModule,
    MatIconModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSelectModule,
    
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  entryComponents:[],
  providers: []
})
export class WizardModule { }
