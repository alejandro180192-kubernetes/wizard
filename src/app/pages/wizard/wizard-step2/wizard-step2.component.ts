import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { WizardService } from 'src/services/wizard.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-wizard-step2',
  templateUrl: './wizard-step2.component.html',
  styleUrls: ['./wizard-step2.component.scss']
})
export class WizardStep2Component implements OnInit {
  categories$: Observable<Array<{ id: number, name: string }>>
  subcategories$: Observable<Array<{ id: number, name: string }>>
  states$: Observable<Array<{ id: number, name: string }>>
  cities$: Observable<Array<{ id: number, name: string }>>
  neighborhoods$: Observable<Array<{ id: number, name: string }>>

  step2Form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private location: Location,
    private service: WizardService) {
  }

  ngOnInit() {
    this.step2Form = this.formBuilder.group({
      businessName: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: ['', Validators.required],
      email: ['', Validators.required],
      celphone: ['', Validators.required],
      phone: ['', Validators.required],
      facebook: ['', Validators.required],
      twitter: ['', Validators.required],
      instagram: ['', Validators.required],
      state: [null, Validators.required],
      city: ['', Validators.required],
      neighborhood: ['', Validators.required],
      streetName: ['', Validators.required],
      number: ['', Validators.required],
      zipcode: ['', Validators.required],
      location: ['', Validators.required]
    });
    
    this.states$ = this.service.getStates();
    this.categories$ = this.service.getBusinessCategories();
  }

  onCategoryChange(event: any) {
    console.log("EVENT", event);
    this.subcategories$ = this.service.getBusinessSubcategories(event.value);
  }

  onStateChange(event: any) {
    console.log("EVENT", event);
    this.cities$ = this.service.getCities(event.value);
  }

  onCityChange(event: any) {
    console.log("EVENT", event);
    this.neighborhoods$ = this.service.getNeighborhoods(event.value);
    this.neighborhoods$.subscribe(n => console.log("N", n));
  }

  cancel() {
    this.location.back();
  }

  continue() {
    console.log(this.step2Form.value);
    
    this.router.navigateByUrl('/wizard/step3')
  }
}