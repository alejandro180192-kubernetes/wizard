import { Component, OnInit, HostBinding } from '@angular/core';
import { Router } from '@angular/router';
import { WizardService } from 'src/services/wizard.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TenantService, Tenant } from '../../../shared/tenant/tenant.service';


@Component({
  selector: 'app-wizard-step1',
  templateUrl: './wizard-step1.component.html',
  styleUrls: ['./wizard-step1.component.scss', './app.component.skins.scss']
})
export class WizardStep1Component implements OnInit{
  destroyed$ = new Subject<boolean>();

  public loading = false;
  
  step1Form: FormGroup;
  
  constructor(private router: Router, private service: WizardService, private formBuilder: FormBuilder, private tenantService: TenantService){
  }

  @HostBinding("class.theme-client1") public client1Theme: boolean;
  @HostBinding("class.theme-client2") public client2Theme: boolean;

  ngOnInit(){
    this.step1Form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, , Validators.email]],
      mobile_phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
    })
    this.loading = true;
    console.log('init app.component');
    
    this.enableThemes();
    /*this.service.getSiteBuilderPersonalData(1).subscribe(r => {
      this.loading = false;
    }, error => {
      console.error("getSiteBuilderPersonalData() ERROR", error);
      
      this.loading = false;
    })*/
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  send(){
    this.loading = true;
    this.service.saveSiteBuilderPersonalData(this.step1Form.value)
    .pipe(takeUntil(this.destroyed$)).subscribe(resp => {
      this.loading = false;
      this.router.navigateByUrl('/wizard/step2')
    }, error => {
      console.error("ERROR", error);
      
      this.loading = false
    });
  }


  get tenant() : string {
    return this.tenantService.getTenant();
  }

  enableThemes() {
    this.client1Theme = this.tenantService.getTenant() === Tenant.CLIENT1;
    this.client2Theme = this.tenantService.getTenant() === Tenant.CLIENT2;
    console.log('enablethemes', this.client1Theme, this.client2Theme);
  }
}