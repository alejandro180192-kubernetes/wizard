import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { WizardStep1Component } from './wizard-step1/wizard-step1.component';
import { WizardStep2Component } from './wizard-step2/wizard-step2.component';
import { WizardStep3Component } from './wizard-step3/wizard-step3.component';
import { WizardStep4Component } from './wizard-step4/wizard-step4.component';

const routes: Routes = [
    {
        path: 'step1',
        component: WizardStep1Component
    },
    {
        path: 'step2',
        component: WizardStep2Component
    },
    {
        path: 'step3',
        component: WizardStep3Component
    },
    {
        path: 'step4',
        component: WizardStep4Component
    },
    {
        path: '',
        redirectTo: 'step1',
        pathMatch: 'full'
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WizardRoutingModule { }