import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WizardCustomRoutingModule } from './wizard-custom-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card'
import { MatButtonModule } from '@angular/material/button'
import {MatGridListModule} from '@angular/material/grid-list';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';
import { MatIconModule} from '@angular/material/icon';
import { MatRadioModule} from '@angular/material/radio';
import { MatCheckboxModule} from '@angular/material/checkbox';
import { MatSelectModule} from '@angular/material/select';

import {MatStepperModule} from '@angular/material/stepper';
import { ComponentsModule } from '../../components/components.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    WizardCustomRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatStepperModule,
    MatIconModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSelectModule,
    
    ReactiveFormsModule,
    FlexLayoutModule
  ]
})
export class WizardCustomModule { }

