import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WizardStep1Component } from './wizard-step1/wizard-step1.component';


const routes: Routes = [
  {
    path: 'step1',
    component: WizardStep1Component
  },
  {
    path: '',
    redirectTo: 'step1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WizardCustomRoutingModule { }
