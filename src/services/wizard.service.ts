import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { WizardRepository } from 'src/repositories/wizard.repository';
import { IWizardService } from 'src/core/contracts/IWizard.service';


@Injectable()
export class WizardService implements IWizardService {
    constructor(
        private repository: WizardRepository, ) { }

    getSiteBuilderPersonalData(businessId: number): Observable<any> {
        console.log("--- Executing: WizardService.getSiteBuilderPersonalData()");

        return this.repository.getSiteBuilderPersonalData(businessId).pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getSiteBuilderPersonalData()");
                throw error;
            })
        )
    }

    saveSiteBuilderPersonalData(payload: any): Observable<any> {
        console.log("--- Executing: WizardService.saveSiteBuilderPersonalData()");

        return this.repository.saveSiteBuilderPersonalData(payload).pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.saveSiteBuilderPersonalData()");
                throw error;
            })
        )
     }

    getStates(): Observable<any> {
        console.log("--- Executing: WizardService.getStates()");

        return this.repository.getStates().pipe(
            map((response) => {
                //console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getStates()");
                throw error;
            })
        )
    }

    getCities(stateId: number): Observable<Array<any>> {
        console.log("--- Executing: WizardService.getCities()");

        return this.repository.getCities(stateId).pipe(
            map((response) => {
                //console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getCities()");
                throw error;
            })
        )
    }

    getNeighborhoods(cityId: number): Observable<Array<any>> {
        console.log("--- Executing: WizardService.getNeighborhoods()");

        return this.repository.getNeighborhoods(cityId).pipe(
            map((response) => {
                //console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getNeighborhoods()");
                throw error;
            })
        )
    }

    getBusinessCategories(): Observable<Array<any>> {
        console.log("--- Executing: WizardService.getBusinessCategories()");

        return this.repository.getBusinessCategories().pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getBusinessCategories()");
                throw error;
            })
        )
    }

    getBusinessSubcategories(businessId: number): Observable<Array<any>> {
        console.log("--- Executing: WizardService.getBusinessSubcategories()");

        return this.repository.getBusinessSubcategories(businessId).pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getBusinessSubcategories()");
                throw error;
            })
        )
    }

    getBusinessInfo(businessId: number): Observable<Array<any>> {
        console.log("--- Executing: WizardService.getBusinessInfo()");

        return this.repository.getBusinessInfo(businessId).pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getBusinessInfo()");
                throw error;
            })
        )
    }

    saveBusinessInfo(payload: any): Observable<Array<any>> {
        console.log("--- Executing: WizardService.saveBusinessInfo()");

        return this.repository.saveBusinessInfo(payload).pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.saveBusinessInfo()");
                throw error;
            })
        )
    }

    getBusinessProducts(businessId: number): Observable<Array<any>> {
        console.log("--- Executing: WizardService.getBusinessProducts()");

        return this.repository.getBusinessProducts(businessId).pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.getBusinessProducts()");
                throw error;
            })
        )
    }

    saveBusinessProducts(payload: any): Observable<Array<any>> {
        console.log("--- Executing: WizardService.saveBusinessProducts()");

        return this.repository.saveBusinessProducts(payload).pipe(
            map((response) => {
                console.log("--- Response", response);

                return response
            }),
            catchError(error => {
                console.error("--- Executing: WizardService.saveBusinessProducts()");
                throw error;
            })
        )
    }
}