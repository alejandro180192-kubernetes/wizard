import { Observable } from 'rxjs';

export interface IWizardService{
    getSiteBuilderPersonalData(businessId: number): Observable<any>;
    saveSiteBuilderPersonalData(data: any): Observable<any>;

    getStates(): Observable<any>;
    getCities(stateId: number): Observable<Array<any>>;
    getNeighborhoods(cityId: number): Observable<Array<any>>;
    getBusinessCategories(): Observable<Array<any>>;
    getBusinessSubcategories(businessId: number): Observable<Array<any>>;

    getBusinessInfo(businessId: number): Observable<Array<any>>;
    saveBusinessInfo(payload: any): Observable<Array<any>>;

    getBusinessProducts(businessId: number): Observable<Array<any>>;
    saveBusinessProducts(payload: any): Observable<Array<any>>;
}