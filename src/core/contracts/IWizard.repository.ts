import { Observable } from 'rxjs';

export interface IWizardRepository{
    getSiteBuilderPersonalData(businessId: number): Observable<any>;
    saveSiteBuilderPersonalData(data: any): Observable<any>;

    getStates(): Observable<any>;
    getCities(stateId: number): Observable<Array<any>>;
    getNeighborhoods(cityId: number): Observable<Array<any>>;
    getBusinessCategories(): Observable<Array<any>>;
    getBusinessSubcategories(businessId: number): Observable<Array<any>>;

    getBusinessInfo(businessId: number): Observable<Array<any>>;
    saveBusinessInfo(payload: any): Observable<Array<any>>;

    getBusinessProducts(businessId: number): Observable<Array<any>>;
    saveBusinessProducts(payload: any): Observable<Array<any>>;
}

export interface IPersonalDataApiResponse{
    id: number
    domain: string
    name: string
    email: string
    mobile_phone: string
    phone: string
    created_at: string
    wizard_finished_at: string
}

export class IBusinessInfoDataApiResponse{
    id: number
    nane: string
    wizardFinishedAt: string
    streetAddress:string
    businessName: string
    email: string
    mobilePhone: string
    phone: string
    facebookUrl: string
    twitterUrl: string
    instagramUrl: string
    businessDescription: string
    zipcode: string
    settlement: string
    settlementId: number
    township: string
    townshipId: number
    state: string
    stateId: number
    businessCategory: string
    businessCategoryId: number
    businessSubcategory: string
    businessSubcategoryId: number
    siteBuilderStateCode: string
    siteBuilderTemplateId: number
    urlDefaultImage: string
}