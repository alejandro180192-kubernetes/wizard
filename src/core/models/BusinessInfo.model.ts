export class BusinessInfoModel implements IBusinessInfoProps{
    id: number;
    nane: string;
    wizardFinishedAt: string;
    streetAddress: string;
    businessName: string;
    email: string;
    mobilePhone: string;
    phone: string;
    facebookUrl: string;
    twitterUrl: string;
    instagramUrl: string;
    businessDescription: string;
    zipcode: string;
    settlement: string;
    settlementId: number;
    township: string;
    townshipId: number;
    state: string;
    stateId: number;
    businessCategory: string;
    businessCategoryId: number;
    businessSubcategory: string;
    businessSubcategoryId: number;
    siteBuilderStateCode: string;
    siteBuilderTemplateId: number;
    urlDefaultImage: string;

    constructor(data: IBusinessInfoProps){

    }

    fromApi(data){

    }
}

export interface IBusinessInfoProps{
    id: number
    nane: string
    wizardFinishedAt: string
    streetAddress:string
    businessName: string
    email: string
    mobilePhone: string
    phone: string
    facebookUrl: string
    twitterUrl: string
    instagramUrl: string
    businessDescription: string
    zipcode: string
    settlement: string
    settlementId: number
    township: string
    townshipId: number
    state: string
    stateId: number
    businessCategory: string
    businessCategoryId: number
    businessSubcategory: string
    businessSubcategoryId: number
    siteBuilderStateCode: string
    siteBuilderTemplateId: number
    urlDefaultImage: string
}