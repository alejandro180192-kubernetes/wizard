import { IPersonalDataApiResponse } from '../contracts';

export class PersonalData implements IPersonalDataProps{
    id: number;
    domain: string;
    name: string;
    email: string;
    mobilePhone: string;
    phone: string;
    createdAt: string;
    wizardFinishedAt: string;

    constructor(data: IPersonalDataProps){
        this.id = data.id
        this.domain = data.domain
        this.name = data.name
        this.email = data.email
        this.mobilePhone = data.mobilePhone
        this.phone = data.phone
        this.createdAt = data.createdAt
        this.wizardFinishedAt = data.wizardFinishedAt
    }

    static fromApi(data: IPersonalDataApiResponse){
        return new PersonalData({
            id: data.id,
            domain: data.domain,
            name: data.name,
            email: data.email,
            mobilePhone: data.mobile_phone,
            phone: data.mobile_phone,
            createdAt: data.created_at,
            wizardFinishedAt: data.wizard_finished_at
        })
    }
}

export interface IPersonalDataProps{
    id: number
    domain: string
    name: string
    email: string
    mobilePhone: string
    phone: string
    createdAt: string
    wizardFinishedAt: string
}