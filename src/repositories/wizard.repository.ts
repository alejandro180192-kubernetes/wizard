import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from "@angular/core";
import { IWizardRepository } from 'src/core/contracts';


@Injectable()
export class WizardRepository implements IWizardRepository {
    readonly BASE_URL = `${environment.apiBaseUrl}/businesses/${environment.instanceName}/wizard`;

    constructor(private http: HttpClient) { }

    getSiteBuilderPersonalData(businessId: number): Observable<any> {
        return this.http.get<any>(`${this.BASE_URL}/g_sitebuilder_personal_data?businessId=${businessId}`)
    }

    saveSiteBuilderPersonalData(payload: any) {
        
        //const body = this._buildHttpParams(payload).toString();

        return this.http.post<any>(`${this.BASE_URL}/p_sitebuilder_personal_data`, payload)
    }

    getStates(): Observable<any> {
        return this.http.get<any>(`${this.BASE_URL}/states`)
    }

    getCities(stateId: number): Observable<Array<any>> {
        return this.http.get<any>(`${this.BASE_URL}/townships?stateId=${stateId}`)
    }

    getNeighborhoods(cityId: number): Observable<Array<any>> {
        return this.http.get<any>(`${this.BASE_URL}/settlements?townshipId=${cityId}`)
    }

    getBusinessCategories(): Observable<Array<any>> {
        return this.http.get<any>(`${this.BASE_URL}/business_categories`)
    }

    getBusinessSubcategories(businessId: number): Observable<Array<any>> {
        return this.http.get<any>(`${this.BASE_URL}/business_subcategories?categoryId=${businessId}`)
    }

    getBusinessInfo(businessId: number): Observable<Array<any>> {
        return this.http.get<any>(`${this.BASE_URL}/g_business_info?businessId=${businessId}`)
    }
    saveBusinessInfo(payload: any): Observable<Array<any>> {

        const body = this._buildHttpParams(payload).toString();

        return this.http.post<any>(`${this.BASE_URL}/p_business_info`, body)
    }

    getBusinessProducts(businessId: number): Observable<Array<any>> {
        return this.http.get<any>(`${this.BASE_URL}/g_business_products?businessId=${businessId}`)
    }

    saveBusinessProducts(payload: any): Observable<Array<any>> {
        
        const body = this._buildHttpParams(payload).toString();

        return this.http.post<any>(`${this.BASE_URL}/p_business_products`, body)
    }

    private _buildHttpParams(payload: any): HttpParams{
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                    params = params.append(key, payload[key]);
            }
        }

        return params;
    }
}