FROM tiangolo/node-frontend:10 as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
ARG configuration=production
RUN npm run build -- --output-path=./dist/out --configuration $configuration

FROM nginx:1.15
EXPOSE 80
COPY --from=build-stage /app/dist/out/ /usr/share/nginx/html
COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf


# test localhost
# docker build --rm -t wizard:latest .
# docker run -d -p 4200:80 --name wizard wizard
# validata on ip client3.com:4200

# push image to registry
# docker build --rm -t wizard:latest .
# docker tag wizard:latest registry.gitlab.com/alejandro180192-kubernetes/wizard:master
# docker login registry.gitlab.com
# docker push registry.gitlab.com/alejandro180192-kubernetes/wizard:master

# other commands
# docker exec -it ID-CONTAINER ls /usr/share/nginx/html
# docker ps
# docker container stop 3426914376a2
# docker container rm 3426914376a2
